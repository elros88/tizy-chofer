import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.1
import QtMultimedia 5.5

import Material 0.3
import Material.ListItems 0.1 as ListItem

import QZXing 2.3

import "qrc:/controladores/ControladorLector.js" as Controlador

Page
{
    id: lector
    width: parent.width
    height: parent.height
    actionBar.hidden: true

    Camera
    {
        id:camera
        focus {
            focusMode: CameraFocus.FocusContinuous
            focusPointMode: CameraFocus.FocusPointAuto
        }
    }

    VideoOutput
    {
        id: videoOutput
        source: camera
        anchors.fill: parent
        autoOrientation: true
        fillMode: VideoOutput.Stretch
        filters: [ zxingFilter ]
        MouseArea {
            anchors.fill: parent
            onClicked: {
                camera.focus.customFocusPoint = Qt.point(mouse.x / width,  mouse.y / height);
                camera.focus.focusMode = CameraFocus.FocusMacro;
                camera.focus.focusPointMode = CameraFocus.FocusPointCustom;
            }
        }
        Rectangle {
            id: captureZone
            color: "red"
            opacity: 0.2
            width: parent.width / 2
            height: width
            anchors.centerIn: parent

            Text
            {
                text: qsTr("Encuadre el Qr aqui")
                anchors.centerIn: parent
            }
        }
    }

    QZXingFilter
    {
        id: zxingFilter
        captureRect:
        {
            // setup bindings
            videoOutput.contentRect;
            videoOutput.sourceRect;
            return videoOutput.mapRectToSource(videoOutput.mapNormalizedRectToItem(Qt.rect(
                                                                                       0.25, 0.25, 0.5, 0.5
                                                                                       )));
        }

        decoder
        {
            enabledDecoders: QZXing.DecoderFormat_EAN_13 | QZXing.DecoderFormat_CODE_39 | QZXing.DecoderFormat_QR_CODE

            onTagFound: {
                console.log(tag + " | " + decoder.foundedFormat() + " | " + decoder.charSet());
                alerta.open("procesando")
                Controlador.pagar(tag);
            }

            tryHarder: false
        }

        onDecodingStarted:
        {
            //            console.log("started");
        }

        property int framesDecoded: 0
        property real timePerFrameDecode: 0

        onDecodingFinished:
        {
            timePerFrameDecode = (decodeTime + framesDecoded * timePerFrameDecode) / (framesDecoded + 1);
            framesDecoded++;
            console.log("frame finished: " + succeeded, decodeTime, timePerFrameDecode, framesDecoded);
        }
    }
}
