function pagar(token)
{
    restPago.iniciar("http://api.tizy.kstesting.com.ve/transaction");
    restPago.post(pago.crearJSON(token));
    pageStack.push("qrc:/procesando");
}

function recibirRespuestaPago()
{
    if(restPago.validarRespuesta())
    {
        alerta.open("Pago Exitoso");
    }
    else
    {
        alerta.open("Error");
    }

    pageStack.pop("qrc:/procesando");
}
