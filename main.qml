import QtQuick 2.9
import QtQuick.Window 2.2

import Material 0.3
import Material.ListItems 0.1 as ListItem

ApplicationWindow
{
    id: app
    width: 480
    height: 720

    visible: true

    initialPage: "qrc:/lector"

    theme
    {
        id: tema
        primaryColor: "#303f9f"
        accentColor: "#ff4081"
        backgroundColor: "#ffffff"
    }

    Snackbar
    {
        id: alerta
        duration: 7000
    }
}
