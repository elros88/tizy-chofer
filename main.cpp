#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QZXing.h>
#include <restful.h>
#include <pago.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QZXing::registerQMLTypes();

    Restful rest;
    Pago pago;

    QQmlApplicationEngine engine;

    QPM_INIT(engine);

    engine.rootContext()->setContextProperty("restPago", rest.getEndpointPago());
    engine.rootContext()->setContextProperty("pago", &pago);


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
