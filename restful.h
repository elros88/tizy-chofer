#ifndef RESTFUL_H
#define RESTFUL_H

#include <QObject>
#include <rest.h>

class Restful : public QObject
{
    Q_OBJECT
    Rest* endpointPago;

public:
    explicit Restful(QObject *parent = nullptr);


    Rest *getEndpointPago() const;
    void setEndpointPago(Rest *value);

signals:

public slots:
};

#endif // RESTFUL_H
